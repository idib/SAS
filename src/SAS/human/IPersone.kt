package SAS.human

import java.awt.geom.Point2D

/**
 * Created by idib on 17.10.17.
 */
interface IPersone {
    fun go(point2D: Point2D)
    fun work()
}
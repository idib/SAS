package SAS

import javafx.geometry.Point2D

/**
 * Created by idib on 22.10.17.
 */

interface IMovable {
    fun setPosition(point: Point2D)
    fun getPosition(): Point2D
}

interface IMoveControl {
    fun moveTo(point: Point2D, movable: IMovable)
}

class MoveControl:IMoveControl{
    override fun moveTo(point: Point2D, movable: IMovable) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}